package com.team1.shoppy

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class StoreNavigationMain :AppCompatActivity() {

    private lateinit var scanButton : Button

    private lateinit var buttonNext : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.store_navigation)

        scanButton = findViewById(R.id.buttonScan)

        scanButton.setOnClickListener {
            val intent = Intent(this, QR_Scanner::class.java)
            startActivity(intent)
        }


        buttonNext = findViewById(R.id.buttonNext)

        buttonNext.setOnClickListener {

            val intent = Intent(this, PaymentActivity::class.java)
            startActivity(intent)
        }

    }
}