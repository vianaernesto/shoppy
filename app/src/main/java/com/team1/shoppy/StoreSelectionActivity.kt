package com.team1.shoppy

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.fragment.app.FragmentActivity


class StoreSelectionActivity : FragmentActivity() {


    private lateinit var shopButton : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.store_view)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        shopButton = findViewById(R.id.shopButton)
        shopButton.setOnClickListener {
            val intent = Intent(this, ProductsActivity::class.java)
            startActivity(intent)
        }
    }


}
