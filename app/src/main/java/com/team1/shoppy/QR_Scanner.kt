package com.team1.shoppy

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import com.google.android.gms.vision.barcode.Barcode;
import info.androidhive.barcode.BarcodeReader
import android.widget.Toast


class QR_Scanner : AppCompatActivity(), BarcodeReader.BarcodeReaderListener {

    private var barcodeReader: BarcodeReader? = null

    override fun onScannedMultiple(barcodes: MutableList<Barcode>?) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qr__scanner)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // get the barcode reader instance
        barcodeReader =
            supportFragmentManager.findFragmentById(R.id.barcode_scanner) as BarcodeReader
    }
    override fun onScanned(barcode: Barcode) {
        // single barcode scanned
        barcodeReader?.playBeep()
        val myIntent = Intent(
            this@QR_Scanner,
            StoreNavigationSelect::class.java
        )
        startActivity(myIntent)

    }

//    override fun onScannedMultiple(list: List<Barcode>) {
//        // multiple barcodes scanned
//    }

    override fun onBitmapScanned(sparseArray: SparseArray<Barcode>) {
        // barcode scanned from bitmap image
    }

    override fun onScanError(s: String) {
        // scan error
        Toast.makeText(applicationContext, "Error occurred while scanning $s", Toast.LENGTH_SHORT)
            .show()
    }

    override fun onCameraPermissionDenied() {
        // camera permission denied
    }

}
