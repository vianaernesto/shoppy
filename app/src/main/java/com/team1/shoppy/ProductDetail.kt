package com.team1.shoppy

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ProductDetail : AppCompatActivity() {

    private lateinit var buttonAdd : FloatingActionButton

    private lateinit var buttonreject : FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        buttonAdd = findViewById(R.id.floatingActionButton3)

        buttonAdd.setOnClickListener {
            val intent = Intent(this, ProductSelectActivity::class.java)
            startActivity(intent)
        }

        buttonreject = findViewById(R.id.floatingActionButton)

        buttonreject.setOnClickListener {
            val intent = Intent(this, ProductsActivity::class.java)
            startActivity(intent)
        }
    }

}
