package com.team1.shoppy

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class ProductSelectActivity : AppCompatActivity() {

    private lateinit var deleteButton : Button

    private lateinit var detailButton : Button

    private lateinit var buttonNext : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.product_selected_view)

        deleteButton = findViewById(R.id.button_deleteProduct1)

        deleteButton.setOnClickListener {
            val intent = Intent(this, ProductsActivity::class.java)
            startActivity(intent)
        }

        detailButton = findViewById(R.id.button_product1)

        detailButton.setOnClickListener {

            val intent = Intent(this, ProductDetail::class.java)
            startActivity(intent)
        }

        buttonNext = findViewById(R.id.buttonNext)

        buttonNext.setOnClickListener {

            val intent = Intent(this, StoreNavigationMain::class.java)
            startActivity(intent)
        }

    }
}