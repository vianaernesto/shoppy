package com.team1.shoppy

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ReceiptActivity : AppCompatActivity() {

    private lateinit var buttonFinish : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.receipt_main)

        buttonFinish = findViewById(R.id.buttonFinish)

        buttonFinish.setOnClickListener {
            val intent = Intent(this, InitialActivity::class.java)
            startActivity(intent)
        }

    }
}