package com.team1.shoppy

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.widget.Toolbar

class ProductsActivity : AppCompatActivity(){

    private lateinit var toolbar: Toolbar

    private lateinit var buttonDetail : Button

    private lateinit var addButton: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.products_main)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        buttonDetail = findViewById(R.id.button_product1)

        buttonDetail.setOnClickListener {
            val intent = Intent(this, ProductDetail::class.java)
            startActivity(intent)
        }

        addButton = findViewById(R.id.button_addProduct1)

        addButton.setOnClickListener {
            val intent = Intent(this, ProductSelectActivity::class.java)
            startActivity(intent)
        }

    }
}